<?php
    
    // Preencher as credênciais
    $url  = "http://www.healmarketing.com.br/";
    $to   = "contato@healmarketing.com.br";
    $from = "formulario@healmarketing.com.br";

    $name    = $_POST["nome"];
    $email   = $_POST["email"];
    $message = $_POST["mensagem"];

    if (empty($message)) {
        ?>
          <SCRIPT>alert('Preencha a mensagem');window.location = 'index.html';</SCRIPT>
        <?php 
        die();
    }

    if (empty($name)) {
        ?>
            <SCRIPT>alert('Preencha o nome');window.location = 'index.html';</SCRIPT>
        <?php 
        die();
    } else {
        if (!preg_match("/^[a-zA-Z ]*$/",$name)) {
            ?>
            <SCRIPT>alert('Nome invalido');window.location = 'index.html';</SCRIPT>
            <?php 
            die(); 
        }
    }

    if (empty($email)) {
        ?>
            <SCRIPT>alert('Preencha o email');window.location = 'index.html';</SCRIPT>
        <?php 
        die();
    } else {
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            ?>
            <SCRIPT>alert('email invalido');window.location = 'index.html';</SCRIPT>
            <?php 
            die();
        }
    }
    
    $subject = "Nova mensagem Site Heal";
    $body = "<b>Nome:</b> $name<br><b>E-mail:</b> $email<br><b>Mensagem:</b> $message"; // email body message

    $headers  = 'MIME-Version: 1.0' . "\r\n";
    $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
    $headers  .= "From: " . $from . "\r\n";

    mail($to,$subject,$body,$headers);
    file_put_contents("emails.txt", $body . "\n\n", FILE_APPEND);
    
    // redirect
    header("Location:" . $url . "?m=obrigado");
    exit;
?>